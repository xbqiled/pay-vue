/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
'use strict'

var $protobuf = require('protobufjs/light')

var $root = ($protobuf.roots['default'] || ($protobuf.roots['default'] = new $protobuf.Root()))
  .addJSON({
    im: {
      options: {
        optimize_for: 'SPEED',
        java_package: 'com.pay.common.im.packets',
        java_outer_classname: 'MessageResProto'
      },
      nested: {
        Content: {
          fields: {
            text: {
              type: 'string',
              id: 1
            },
            width: {
              type: 'int32',
              id: 2
            },
            higth: {
              type: 'int32',
              id: 3
            },
            duration: {
              type: 'int32',
              id: 4
            },
            payDesc: {
              type: 'string',
              id: 5
            }
          }
        },
        LoginReq: {
          fields: {
            orgId: {
              type: 'string',
              id: 1
            },
            userName: {
              type: 'string',
              id: 3
            },
            password: {
              type: 'string',
              id: 4
            },
            deviceType: {
              type: 'int32',
              id: 5
            },
            deviceId: {
              type: 'string',
              id: 6
            }
          }
        },
        LoginRes: {
          fields: {
            code: {
              type: 'uint32',
              id: 1
            },
            msg: {
              type: 'string',
              id: 2
            },
            token: {
              type: 'string',
              id: 3
            },
            user: {
              type: 'User',
              id: 4
            }
          }
        },
        User: {
          fields: {
            userId: {
              type: 'int32',
              id: 1
            },
            nickName: {
              type: 'string',
              id: 2
            },
            avatar: {
              type: 'string',
              id: 3
            },
            orgId: {
              type: 'string',
              id: 4
            }
          }
        },
        Message: {
          oneofs: {
            pack: {
              oneof: [
                'loginReq',
                'loginRes',
                'messageReq',
                'messageRes'
              ]
            }
          },
          fields: {
            version: {
              type: 'int32',
              id: 1
            },
            command: {
              type: 'int32',
              id: 2
            },
            loginReq: {
              type: 'LoginReq',
              id: 3
            },
            loginRes: {
              type: 'LoginRes',
              id: 4
            },
            messageReq: {
              type: 'MessageReq',
              id: 5
            },
            messageRes: {
              type: 'MessageRes',
              id: 6
            }
          }
        },
        MessageReq: {
          fields: {
            messageId: {
              type: 'string',
              id: 1
            },
            fromUser: {
              type: 'User',
              id: 2
            },
            toUser: {
              type: 'User',
              id: 3
            },
            msgType: {
              type: 'int32',
              id: 4
            },
            content: {
              type: 'Content',
              id: 5
            },
            sendTime: {
              type: 'int64',
              id: 6
            }
          }
        },
        MessageRes: {
          fields: {
            code: {
              type: 'uint32',
              id: 1
            },
            msg: {
              type: 'string',
              id: 2
            },
            messageId: {
              type: 'string',
              id: 3
            },
            acceptTime: {
              type: 'int64',
              id: 4
            }
          }
        }
      }
    }
  })

module.exports = $root
