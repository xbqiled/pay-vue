import request from '@/utils/request'

export function doConfig(data) {
  return request({
    url: 'api/orgchannel',
    method: 'post',
    data
  })
}

export function downloadOrgChannelInfo(params) {
  return request({
    url: 'api/orgchannel/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

export function del(id) {
  return request({
    url: 'api/orgchannel/' + id,
    method: 'delete'
  })
}

export function getAllConfig(id) {
  return request({
    url: 'api/orgchannel/getAllConfig/' + id,
    method: 'get'
  })
}
