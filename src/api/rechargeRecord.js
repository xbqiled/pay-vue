import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/rechargeRecord',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/rechargeRecord/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/rechargeRecord',
    method: 'put',
    data
  })
}

export function downloadRechargeRecord(params) {
  return request({
    url: 'api/rechargeRecord/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

