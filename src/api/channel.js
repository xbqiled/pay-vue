import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/imChannel',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/imChannel/' + id,
    method: 'delete'
  })
}

export function refresh(id) {
  return request({
    url: 'api/imChannel/refresh/' + id,
    method: 'post'
  })
}

export function edit(data) {
  return request({
    url: 'api/imChannel',
    method: 'put',
    data
  })
}

export function downloadChannelInfo(params) {
  return request({
    url: 'api/imChannel/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
