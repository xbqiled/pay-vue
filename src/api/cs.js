import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/cs',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/cs/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/cs',
    method: 'put',
    data
  })
}

export function download(params) {
  return request({
    url: 'api/cs/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

