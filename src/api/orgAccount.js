import request from '@/utils/request'

export function getConfig() {
  return request({
    url: 'api/orgchannel/getConfig/',
    method: 'get'
  })
}

export function add(data) {
  return request({
    url: 'api/orgAccount',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/orgAccount/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/orgAccount',
    method: 'put',
    data
  })
}
export function addPoint(data) {
  return request({
    url: 'api/orgAccount/addPoint',
    method: 'post',
    data
  })
}
export function recharge(data) {
  return request({
    url: 'api/orgAccount/recharge',
    method: 'post',
    data
  })
}

export function downloadTraderOrgAccount(params) {
  return request({
    url: 'api/orgAccount/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

