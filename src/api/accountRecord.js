import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/accountRecord',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/accountRecord/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/accountRecord',
    method: 'put',
    data
  })
}

export function downloadTraderAccountRecord(params) {
  return request({
    url: 'api/accountRecord/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

