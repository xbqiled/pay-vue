import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/imOrg',
    method: 'put',
    data
  })
}

export function edit(data) {
  return request({
    url: 'api/imOrg',
    method: 'put',
    data
  })
}

export function downloadOrgInfo(params) {
  return request({
    url: 'api/imOrg/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
