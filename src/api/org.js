import request from '@/utils/request'

export function getOrgs(params) {
  return request({
    url: 'api/org',
    method: 'get',
    params
  })
}

export function add(data) {
  return request({
    url: 'api/org',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/org/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/org',
    method: 'put',
    data
  })
}

export function downloadDept(params) {
  return request({
    url: 'api/org/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

