import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/imUserInfo',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/imUserInfo/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/imUserInfo',
    method: 'put',
    data
  })
}

export function downloadImUserInfo(params) {
  return request({
    url: 'api/imUserInfo/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
