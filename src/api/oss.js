import request from '@/utils/request'

export function getOss() {
  return request({
    url: 'api/oss',
    method: 'get'
  })
}

export function downloadOss(params) {
  return request({
    url: 'api/oss/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

export function add(data) {
  return request({
    url: 'api/oss',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/oss/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/oss',
    method: 'put',
    data
  })
}
