import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/replyConfig',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/replyConfig/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/replyConfig',
    method: 'put',
    data
  })
}

export function downloadReplyConfig(params) {
  return request({
    url: 'api/replyConfig/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

