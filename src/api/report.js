import request from '@/utils/request'

export function downloadStaticConfig(params) {
  return request({
    url: 'api/report/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

export function downloadChannelStaticConfig(params) {
  return request({
    url: 'api/channelReport/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
