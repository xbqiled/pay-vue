// eslint-disable-next-line no-unused-vars
import request from '../../../utils/request'
// 登录用户信息
export function getChatHis(query) {
  return request({
    url: 'api/chatApi/getChatHis',
    method: 'post',
    data: query
  })
}

export function getUserChatHis(query) {
  return request({
    url: 'api/chatApi/getUserChatHis',
    method: 'post',
    data: query
  })
}

export function uploadImg(query) {
  return request({
    url: 'api/chatApi/imgUpload',
    method: 'post',
    params: query
  })
}

export function uploadFile(query) {
  return request({
    url: 'api/chatApi/fileUpload',
    method: 'post',
    params: query
  })
}
