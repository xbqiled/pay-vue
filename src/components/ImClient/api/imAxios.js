import axios from 'axios'
axios.defaults.withCredentials = true

// 获取用户信息
export function create(apiBaseUrl) {
  // 创建axios实例
  const service = axios.create({
    baseURL: apiBaseUrl, // api的base_url
    timeout: 5000, // 请求超时时间
    withCredentials: true
  })

  service.interceptors.request.use(
    config => {
      return config
    },
    error => {
      Promise.reject(error)
    }
  )

  service.interceptors.response.use(response => {
    if (response.data.code) {
      if (response.data.code === 1) {
        // 重新登录
      }
    }
    return response.data
  },
  error => {
    return Promise.reject(error)
  })
  return service
}
